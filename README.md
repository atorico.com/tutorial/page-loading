
Managing Cookies on Page Load
============================


![Gif](https://s3.io.atorico.com/public/Peek%202021-06-02%2010-35.gif)

Get Started
------------

Start the container:

```
docker-compose up
```

Then open browser to [http://localhost:8080](http://localhost:8080)
